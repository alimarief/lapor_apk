part of 'shared.dart';

dynamic getHourTime(time) {
  return DateFormat.jm().format(time);
}
