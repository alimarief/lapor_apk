part of 'shared.dart';

const double defaultMargin = 24;

//COLOR
Color accentColor1 = Color(0xFF514CD9);
Color accentColor2 = Color(0xFFEF5A5C);
Color accentColor3 = Color(0xFFADADAD);
Color accentColor4 = Color(0xFFFF9952);
Color accentColor5 = Color(0xFFFFF7F2);
// Color accentColor6 = Color(0xFFFF9952);
Color accentColor7 = Color(0xFFF7F6FF);
Color accentColor8 = Color(0xFFF7ECEE);
Color accentColor9 = Color(0xFF20C29B);

// TEXT
TextStyle blackTextFont = GoogleFonts.inter()
    .copyWith(color: Color(0xFF1C1B3A), fontWeight: FontWeight.w400);
TextStyle purpleTextFont = GoogleFonts.inter()
    .copyWith(color: accentColor1, fontWeight: FontWeight.w500);
TextStyle greyTextFont = GoogleFonts.inter()
    .copyWith(color: accentColor3, fontWeight: FontWeight.w500);
TextStyle whiteTextFont = GoogleFonts.inter()
    .copyWith(color: Colors.white, fontWeight: FontWeight.w400);
TextStyle greenTextFont = GoogleFonts.inter()
    .copyWith(color: Color(0xFF20C29B), fontWeight: FontWeight.w400);
TextStyle orangeTextFont = GoogleFonts.inter()
    .copyWith(color: Color(0xFFE78039), fontWeight: FontWeight.w400);
