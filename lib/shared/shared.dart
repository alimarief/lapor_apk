import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

part 'shared_value.dart';
part 'theme.dart';
part 'shared_methods.dart';
