import 'package:flutter/material.dart';
import 'package:lapor/ui/pages/pages.dart';

void main() async {
  runApp(LaporApp());
}

class LaporApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(body: LoginNIKPage()),
    );
  }
}
