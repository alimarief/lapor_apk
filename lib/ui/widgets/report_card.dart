part of 'widgets.dart';

class ReportCard extends StatefulWidget {
  final Report report;
  final Function onTap;

  ReportCard(this.report, {this.onTap});

  @override
  _ReportCardState createState() => _ReportCardState();
}

class _ReportCardState extends State<ReportCard> {
  bool isSend;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (widget.onTap != null) {
          widget.onTap();
        }
      },
      child: Container(
        height: 238,
        width: 334,
        padding: EdgeInsets.symmetric(horizontal: 23, vertical: 15),
        decoration: BoxDecoration(
          color: accentColor7,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            (widget.report.status == true)
                ? BoxShadow(
                    color: Colors.grey,
                    blurRadius: 4,
                    offset: Offset(0, 5),
                  )
                : BoxShadow(),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  widget.report.dateTime.dateAndTime,
                  style: greyTextFont.copyWith(fontSize: 12),
                ),
                Text(
                  getHourTime(dummyReports[0].dateTime),
                  style: greyTextFont.copyWith(
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            SizedBox(height: 9),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.65,
              child: Text(
                widget.report.title,
                style: blackTextFont.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: 9),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.75,
              child: Text(
                widget.report.description,
                style: blackTextFont.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                ),
                maxLines: 3,
              ),
            ),
            (widget.report.status == true) ? SizedBox(height: 15) : SizedBox(),
            (widget.report.status == true)
                ? Container(
                    height: 27,
                    width: 108,
                    decoration: BoxDecoration(
                      color: accentColor8,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Center(
                      child: Text(
                        "Sedang ditinjau",
                        style: orangeTextFont.copyWith(fontSize: 12),
                      ),
                    ),
                  )
                : SizedBox(),
            SizedBox(height: 10),
            (widget.report.status == true)
                ? Container(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: accentColor9,
                        ),
                        SizedBox(width: 5),
                        Text(
                          "Laporan Berhasil Dikirim!",
                          style: greenTextFont.copyWith(
                              fontSize: 12, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  )
                : Container(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.close,
                          color: accentColor2,
                        ),
                        SizedBox(width: 5),
                        Text(
                          "Laporan Gagal Dikirim",
                          style: greenTextFont.copyWith(
                              color: accentColor2,
                              fontSize: 12,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
            // BUTTON RESEND
            // SizedBox(height: 10),
            (widget.report.status == false)
                ? GestureDetector(
                    onTap: () {},
                    child: Center(
                      child: Container(
                        height: 50,
                        width: 350,
                        decoration: BoxDecoration(
                          color: accentColor1,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Center(
                          child: Text(
                            "KIRIM ULANG",
                            style: whiteTextFont.copyWith(
                                fontSize: 13, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
