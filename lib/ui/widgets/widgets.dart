import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import 'package:lapor/models/models.dart';
import 'package:lapor/shared/shared.dart';
import 'package:lapor/extensions/extensions.dart';

part 'hot_news_card.dart';
part 'report_card.dart';
part 'detail_report_card.dart';
part 'bar_chart.dart';
