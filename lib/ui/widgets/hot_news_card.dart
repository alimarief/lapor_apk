part of 'widgets.dart';

class HotCard extends StatelessWidget {
  final HotNews hotNews;

  HotCard(this.hotNews);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 138,
      width: 274,
      child: Stack(
        children: <Widget>[
          Container(
            height: 138,
            width: 274,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: AssetImage(hotNews.image),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            height: 138,
            width: 274,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.34),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  hotNews.title,
                  maxLines: 3,
                  style: whiteTextFont.copyWith(
                      fontSize: 14, fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 12),
                Text(
                  hotNews.date.dateAndTime,
                  style: whiteTextFont.copyWith(
                      fontSize: 12, fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
