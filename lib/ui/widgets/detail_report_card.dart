part of 'widgets.dart';

class DetailReportCard extends StatelessWidget {
  final String reportImage;

  DetailReportCard(this.reportImage);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 102,
      width: 100,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        image: DecorationImage(
          image: AssetImage(reportImage),
        ),
      ),
    );
  }
}
