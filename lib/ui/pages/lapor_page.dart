part of 'pages.dart';

class LaporPage extends StatefulWidget {
  final String imagePath;

  LaporPage({this.imagePath});

  @override
  _LaporPageState createState() => _LaporPageState();
}

class _LaporPageState extends State<LaporPage> {
  TextEditingController titleController = TextEditingController();
  TextEditingController detailController = TextEditingController();

  bool isTitleValid = false;
  bool isDetailValid = false;
  bool isPhotoValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(defaultMargin, 20, defaultMargin, 0),
              child: Stack(
                children: <Widget>[
                  // HEADER
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back,
                        color: accentColor1,
                      ),
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        Text(
                          "BUAT LAPORAN",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // DIVIDER
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: Divider(
                color: Color(0xFFE4E4E4),
                thickness: 1,
              ),
            ),
            // CONTENT
            Container(
              padding: EdgeInsets.fromLTRB(defaultMargin, 27, defaultMargin, 0),
              child: Column(
                children: <Widget>[
                  Text(
                    "Sampaikan laporan Anda langsung kepada\nkepolisian daerah Jambi melalui form dibawah ini.",
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 30),
                  TextField(
                    onChanged: (value) {
                      setState(() {});
                    },
                    controller: titleController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      hintText: "Masukkan judul laporan",
                    ),
                  ),
                  SizedBox(height: 14),
                  Container(
                    height: 200,
                    padding: EdgeInsets.fromLTRB(10, 10, 15, 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(right: 25),
                          child: TextField(
                            keyboardType: TextInputType.name,
                            maxLines: 9,
                            decoration: InputDecoration.collapsed(
                                hintText: "Masukkan detail kejadian"),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Icon(
                            Icons.mic_none,
                            color: accentColor1,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 14),
                  GestureDetector(
                    onTap: () async {
                      WidgetsFlutterBinding.ensureInitialized();
                      final cameras = await availableCameras();
                      final firstCamera = cameras.first;

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PhotoPage(firstCamera)),
                      );
                    },
                    child: Container(
                      height: 50,
                      padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: accentColor1),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.camera_alt_outlined,
                            color: accentColor1,
                          ),
                          SizedBox(width: 10),
                          Text(
                            "TAMBAHKAN FOTO",
                            style: purpleTextFont.copyWith(fontSize: 13),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 50,
                    width: 50,
                    child: (widget.imagePath == null)
                        ? SizedBox()
                        : Image.file(
                            File(widget.imagePath),
                          ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SuccessPage()),
          );
        },
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: (isTitleValid && isDetailValid)
                ? accentColor1
                : Color(0xFFE4E4E4),
          ),
          child: Center(
            child: Text(
              'LAPORAN',
              style: blackTextFont.copyWith(
                color: (isTitleValid && isDetailValid)
                    ? Colors.white
                    : Color(0xFFBEBEBE),
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
