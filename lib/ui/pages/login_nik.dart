part of 'pages.dart';

class LoginNIKPage extends StatefulWidget {
  @override
  _LoginNIKPageState createState() => _LoginNIKPageState();
}

class _LoginNIKPageState extends State<LoginNIKPage> {
  TextEditingController nikController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isNIKValid = false;
  bool isPasswordValid = false;
  FlutterOtp otp = FlutterOtp();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 100),
                  SizedBox(
                    height: 101,
                    width: 101,
                    child: Image.asset('assets/police_icon_login.png'),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 18, bottom: 14),
                    child: Text(
                      "LOGIN KE ANGSO DUO APP",
                      style: blackTextFont.copyWith(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 23),
                    child: Text(
                      "Silahkan masuk ke Angso Duo App untuk\nmelaporkan kegiatan kriminal, kasus covid, dan\nkebakaran hutan di sekitar anda. ",
                      style: blackTextFont.copyWith(
                        fontSize: 13,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      setState(() {
                        isNIKValid = value.length == 13;
                      });
                    },
                    controller: nikController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Masukkan NIK",
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    obscureText: true,
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      setState(() {
                        isPasswordValid =
                            value.length >= 10 && value.length <= 12;
                      });
                    },
                    controller: passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Masukkan Password",
                    ),
                  ),
                  SizedBox(height: 16),
                  Row(
                    children: <Widget>[
                      Text(
                        "Belum punya akun? ",
                        style: blackTextFont.copyWith(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegisterPage()),
                          );
                        },
                        child: Text(
                          "Daftar disini",
                          style: purpleTextFont.copyWith(fontSize: 13),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () async {
          if (isNIKValid && isPasswordValid) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => OtpPage(
                  phoneNumber: passwordController.text,
                ),
              ),
            );
          } else {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "User/password anda tidak sesuai",
            )..show(context);
          }
        },
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: (isNIKValid && isPasswordValid)
                ? accentColor1
                : Color(0xFFE4E4E4),
          ),
          child: Center(
            child: Text(
              'LOGIN',
              style: blackTextFont.copyWith(
                color: (isNIKValid && isPasswordValid)
                    ? Colors.white
                    : Color(0xFFBEBEBE),
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
