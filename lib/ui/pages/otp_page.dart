part of 'pages.dart';

class OtpPage extends StatefulWidget {
  final String phoneNumber;

  OtpPage({@required this.phoneNumber});

  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  bool isOTPvalid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 100),
                  SizedBox(
                    height: 101,
                    width: 101,
                    child: Image.asset('assets/lock_icon.png'),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 18, bottom: 14),
                    child: Text(
                      "VERIFIKASI OTP",
                      style: blackTextFont.copyWith(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 23),
                    child: Text(
                      "Masukkan kode OTP yang dikirimkan ke nomor\nhandphone anda : ${widget.phoneNumber}",
                      style: blackTextFont.copyWith(
                        fontSize: 13,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  OTPTextField(
                    length: 4,
                    width: 250,
                    fieldWidth: 50,
                    style: TextStyle(fontSize: 17),
                    textFieldAlignment: MainAxisAlignment.spaceEvenly,
                    fieldStyle: FieldStyle.box,
                    onCompleted: (pin) {
                      setState(() {
                        isOTPvalid = pin.length == 4;
                        print("COMPLETED " + pin);
                      });
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          if (isOTPvalid) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LoginLaporPage()),
            );
          } else {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Data yang kamu inputkan tidak sesuai",
            )..show(context);
          }
        },
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: (isOTPvalid) ? accentColor1 : Color(0xFFE4E4E4),
          ),
          child: Center(
            child: Text(
              'VERIFIKASI',
              style: blackTextFont.copyWith(
                color: (isOTPvalid) ? Colors.white : Color(0xFFBEBEBE),
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
