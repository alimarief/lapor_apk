part of 'pages.dart';

class HistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(defaultMargin, 20, defaultMargin, 0),
              child: Stack(
                children: <Widget>[
                  // HEADER
                  Center(
                    child: Column(
                      children: [
                        Text(
                          "RIWAYAT LAPORAN",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // DIVIDER
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: Divider(
                color: Color(0xFFE4E4E4),
                thickness: 1,
              ),
            ),
            // CONTENT
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: defaultMargin,
                vertical: 15,
              ),
              child: ListView.builder(
                // scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: dummyReports.length,
                itemBuilder: (_, index) => Container(
                  margin: EdgeInsets.only(
                      top: (index == 0) ? 0 : 0,
                      bottom: (index == dummyReports.length - 1)
                          ? defaultMargin
                          : 16),
                  child: ReportCard(
                    dummyReports[index],
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                DetailReportPage(dummyReports[index])),
                      );
                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 100),
          ],
        ),
      ),
    );
  }
}
