import 'dart:io';

import 'package:camera/camera.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:lapor/models/models.dart';
import 'package:lapor/shared/shared.dart';
import 'package:lapor/ui/widgets/widgets.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:lapor/extensions/extensions.dart';

import 'package:flutter_otp/flutter_otp.dart';

part 'login_nik.dart';
part 'main_page.dart';
part 'register_page.dart';
part 'otp_page.dart';
part 'login_lapor.dart';
part 'dashboard_page.dart';
part 'history_page.dart';
part 'lapor_page.dart';
part 'success_page.dart';
part 'photo_page.dart';
part 'profile_page.dart';
part 'detail_report_page.dart';
