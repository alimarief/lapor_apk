part of 'pages.dart';

class SuccessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => MainPage()),
        );
        return;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 72,
                  width: 72,
                  child: Image.asset("assets/check_icon.png"),
                ),
                SizedBox(height: 27),
                Text(
                  "LAPORAN SUKSES TERKIRIM",
                  style: blackTextFont.copyWith(
                      fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 27),
                Text(
                  "Terima kasih atas laporan anda. Command center\npolda Jambi akan menindaklanjuti laporan anda\nsesegera mungkin.",
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
