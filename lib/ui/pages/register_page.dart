part of 'pages.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController nikController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isNIKValid = false;
  bool isPasswordValid = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 100),
                  SizedBox(
                    height: 101,
                    width: 101,
                    child: Image.asset('assets/person_icon.png'),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 18, bottom: 14),
                    child: Text(
                      "REGISTRASI AKUN",
                      style: blackTextFont.copyWith(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 23),
                    child: Text(
                      "Silahkan daftar ke Angso Duo App untuk\nmelaporkan kegiatan kriminal, kasus covid, dan\nkebakaran hutan di sekitar anda. ",
                      style: blackTextFont.copyWith(
                        fontSize: 13,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      setState(() {
                        isNIKValid = value.length == 13;
                      });
                    },
                    controller: nikController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Masukkan NIK",
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      setState(() {
                        isPasswordValid =
                            value.length >= 10 && value.length <= 12;
                      });
                    },
                    controller: passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Masukkan Nomor Telepon",
                    ),
                  ),
                  SizedBox(height: 16),
                  Row(
                    children: <Widget>[
                      Text(
                        "Belum punya akun? ",
                        style: blackTextFont.copyWith(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginNIKPage()),
                          );
                        },
                        child: Text(
                          "Login disini",
                          style: purpleTextFont.copyWith(fontSize: 13),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          if (isNIKValid && isPasswordValid) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => OtpPage(
                  phoneNumber: passwordController.text,
                ),
              ),
            );
          } else {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Data yang kamu inputkan tidak sesuai",
            )..show(context);
          }
        },
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: (isNIKValid && isPasswordValid)
                ? accentColor1
                : Color(0xFFE4E4E4),
          ),
          child: Center(
            child: Text(
              'REGISTER',
              style: blackTextFont.copyWith(
                color: (isNIKValid && isPasswordValid)
                    ? Colors.white
                    : Color(0xFFBEBEBE),
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
