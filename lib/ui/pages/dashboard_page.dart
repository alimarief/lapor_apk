part of 'pages.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  bool isClick = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(defaultMargin, 20, defaultMargin, 0),
              child: Stack(
                children: <Widget>[
                  // HEADER
                  GestureDetector(
                    onTap: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => ProfilePage()),
                      // );
                      setState(() {
                        // isClick = true;
                        isClick ? isClick = false : isClick = true;
                      });
                    },
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        height: 35,
                        width: 35,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/person_icon.png"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        Text(
                          "LAPOR MASYARAKAT",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          DateTime.now().dateAndTime,
                          style: blackTextFont.copyWith(
                              fontSize: 11, fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // DIVIDER
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    bottom: 10,
                    top: 5,
                  ),
                  child: Divider(
                    color: Color(0xFFE4E4E4),
                    thickness: 1,
                  ),
                ),
                // STATISTIC
                Container(
                  padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                  margin: EdgeInsets.only(top: 30, bottom: 19),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Statistik COVID-19",
                        style: blackTextFont.copyWith(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 192,
                        width: 343,
                        child: BarChartCovid(),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            children: <Widget>[
                              Text(
                                "Total Kasus",
                                style: blackTextFont.copyWith(fontSize: 12),
                              ),
                              Text(
                                dummyCovid[0].suspect.toString(),
                                style: blackTextFont.copyWith(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            height: 40,
                            width: 1,
                            color: Color(0xFFD7D7D7),
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "Sembuh",
                                style: blackTextFont.copyWith(fontSize: 12),
                              ),
                              Text(
                                dummyCovid[0].recover.toString(),
                                style: blackTextFont.copyWith(
                                    color: accentColor1,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            height: 40,
                            width: 1,
                            color: Color(0xFFD7D7D7),
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "Meninggal",
                                style: blackTextFont.copyWith(fontSize: 12),
                              ),
                              Text(
                                dummyCovid[0].died.toString(),
                                style: blackTextFont.copyWith(
                                    color: accentColor4,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                //BOX PROFIL
                (isClick)
                    ? SizedBox()
                    : Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          margin:
                              EdgeInsets.only(top: 15, right: defaultMargin),
                          height: 81,
                          width: 163,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 4,
                                offset: Offset(0, 5),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProfilePage()),
                                  );
                                },
                                child: Container(
                                  padding: EdgeInsets.only(top: 10, left: 15),
                                  child: Text(
                                    "Profil",
                                    style: blackTextFont.copyWith(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(
                                  vertical: 5,
                                ),
                                child: Divider(
                                  color: Color(0xFFE4E4E4),
                                  thickness: 1,
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginNIKPage()),
                                  );
                                },
                                child: Container(
                                  padding: EdgeInsets.only(left: 15, bottom: 5),
                                  child: Text(
                                    "Logout",
                                    style: blackTextFont.copyWith(
                                        color: accentColor2,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
              ],
            ),

            // POLUSI
            Container(
              height: 135,
              width: 334,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              decoration: BoxDecoration(
                color: accentColor5,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Container(
                padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Kualitas Udara",
                      style: blackTextFont.copyWith(
                          fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 21,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 55,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 6, vertical: 5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: accentColor4,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "AQI US",
                                    style: whiteTextFont.copyWith(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                  SizedBox(height: 2),
                                  Text("72",
                                      style:
                                          whiteTextFont.copyWith(fontSize: 17)),
                                ],
                              ),
                            ),
                            SizedBox(width: 16),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Tingkat Polusi",
                                  style: blackTextFont.copyWith(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                Text("Sedang",
                                    style: blackTextFont.copyWith(
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold,
                                    )),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 36,
                          width: 40,
                          child: Image.asset("assets/cloud.png"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // MAP
            Container(
              padding: EdgeInsets.only(left: 24),
              margin: EdgeInsets.only(top: 29),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Kebakaran Hutan & Lahan",
                    style: blackTextFont.copyWith(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 150,
                        width: 210,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/map.png"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 31),
                      Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Luas Kebakaran",
                                  style: blackTextFont.copyWith(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  dummyFire[0].fireArea.toString(),
                                  style: blackTextFont.copyWith(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Jumlah Hotspot",
                                  style: blackTextFont.copyWith(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  dummyFire[0].hotSpot.toString(),
                                  style: blackTextFont.copyWith(
                                    color: accentColor4,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Jumlah FireSpot",
                                  style: blackTextFont.copyWith(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  dummyFire[0].fireSpot.toString(),
                                  style: blackTextFont.copyWith(
                                    color: accentColor2,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // Hot News
            Container(
              margin: EdgeInsets.only(top: defaultMargin, bottom: 14),
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Text(
                "Berita Terkini",
                style: blackTextFont.copyWith(
                    fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),

            SizedBox(
              height: 160,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: dummyHotNews.length,
                itemBuilder: (_, index) => Container(
                  margin: EdgeInsets.only(
                      left: (index == 0) ? defaultMargin : 0,
                      right: (index == dummyHotNews.length - 1)
                          ? defaultMargin
                          : 16),
                  child: HotCard(dummyHotNews[index]),
                ),
              ),
            ),

            SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
    );
  }
}
