part of 'pages.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int bottomNavBarIndex = 0;
  PageController pageController;

  @override
  void initState() {
    super.initState();

    // bottomNavBarIndex = widget.bottomNavBarIndex;
    pageController = PageController(initialPage: bottomNavBarIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: Container(color: Colors.white),
          ),
          PageView(
            controller: pageController,
            onPageChanged: (index) {
              setState(() {
                bottomNavBarIndex = index;
              });
            },
            children: <Widget>[
              DashboardPage(),
              HistoryPage(),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 60,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: BottomNavigationBar(
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  selectedItemColor: Colors.black,
                  unselectedItemColor: Color(0xFFE5E5E5),
                  currentIndex: bottomNavBarIndex,
                  onTap: (index) {
                    setState(() {
                      bottomNavBarIndex = index;
                      pageController.jumpToPage(index);
                    });
                  },
                  items: [
                    BottomNavigationBarItem(
                      label: 'Dasboard',
                      icon: SizedBox(
                        height: 19,
                        width: 19,
                        child: Container(
                            height: 20, child: Icon(Icons.dashboard_outlined)),
                      ),
                    ),
                    BottomNavigationBarItem(
                      label: 'Riwayat',
                      icon: SizedBox(
                        height: 19,
                        width: 19,
                        child: Icon(Icons.history_outlined),
                      ),
                    ),
                  ]),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LaporPage()),
                );
              },
              child: Container(
                alignment: Alignment.bottomCenter,
                height: 100,
                width: 100,
                child: SizedBox(
                  child: Image.asset("assets/button_lapor.png"),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
