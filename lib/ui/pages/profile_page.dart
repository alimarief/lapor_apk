part of 'pages.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isNameValid = false;
  bool isPhoneValid = false;
  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isEdit = true;
  String titleButton = "EDIT PROFILE";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(defaultMargin, 20, defaultMargin, 0),
              child: Stack(
                children: <Widget>[
                  // HEADER
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back,
                        color: accentColor1,
                      ),
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        Text(
                          isEdit ? "PROFIL" : "EDIT PROFIL",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // DIVIDER
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: Divider(
                color: Color(0xFFE4E4E4),
                thickness: 1,
              ),
            ),
            // CONTENT
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 20),
                  Stack(
                    children: <Widget>[
                      // SizedBox(
                      //   height: 101,
                      //   width: 101,
                      //   child: Image.asset('assets/person_icon.png'),
                      // ),
                      Container(
                        height: 94,
                        width: 94,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/person_icon.png'),
                              fit: BoxFit.cover),
                        ),
                      ),
                      (isEdit)
                          ? Container(
                              height: 94,
                              width: 94,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                              ))
                          : Container(
                              height: 94,
                              width: 94,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.black.withOpacity(0.20),
                              ),
                              child: Center(
                                child: SizedBox(
                                  child: Image.asset("assets/camera_icon.png"),
                                ),
                              ),
                            ),
                    ],
                  ),
                  SizedBox(height: 33),
                  TextField(
                    readOnly: isEdit ? true : false,
                    style:
                        TextStyle(color: isEdit ? Colors.grey : Colors.black),
                    keyboardType: TextInputType.name,
                    onChanged: (value) {
                      setState(() {
                        isNameValid = value != null;
                      });
                    },
                    controller: nameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      hintText: "Masukkan Nama",
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    readOnly: isEdit ? true : false,
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      setState(() {
                        isPhoneValid = value != null &&
                            value.length >= 10 &&
                            value.length <= 12;
                      });
                    },
                    controller: phoneController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      hintText: "Masukkan Nomor Telepon",
                    ),
                    style:
                        TextStyle(color: isEdit ? Colors.grey : Colors.black),
                  ),
                  SizedBox(height: 16),
                  TextField(
                    readOnly: isEdit ? true : false,
                    style:
                        TextStyle(color: isEdit ? Colors.grey : Colors.black),
                    keyboardType: TextInputType.emailAddress,
                    onChanged: (value) {
                      setState(() {
                        isEmailValid = EmailValidator.validate(value);
                      });
                    },
                    controller: emailController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      hintText: "Masukkan Email",
                    ),
                  ),
                  SizedBox(height: 16),
                  TextField(
                    readOnly: isEdit ? true : false,
                    style:
                        TextStyle(color: isEdit ? Colors.grey : Colors.black),
                    obscureText: true,
                    onChanged: (value) {
                      setState(() {
                        isPasswordValid = value != null;
                      });
                    },
                    controller: passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      hintText: "Masukkan Password",
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 14),
            (isEdit)
                ? Container()
                : Container(
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child: Text(
                      "Klik tombol simpan di bawah untuk menyimpan\ndata",
                      style: blackTextFont.copyWith(
                          fontSize: 13, fontWeight: FontWeight.w500),
                    ),
                  ),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          setState(() {
            isEdit ? isEdit = false : isEdit = true;
          });
        },
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: accentColor1,
          ),
          child: Center(
            child: Text(
              isEdit ? "EDIT PROFILE" : "SIMPAN",
              style: blackTextFont.copyWith(
                color: Colors.white,
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
