part of 'pages.dart';

class LoginLaporPage extends StatefulWidget {
  @override
  _LoginLaporPageState createState() => _LoginLaporPageState();
}

class _LoginLaporPageState extends State<LoginLaporPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool isPasswordValid = false;
  bool isNameValid = false;
  String password = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 100),
                  SizedBox(
                    height: 101,
                    width: 101,
                    child: Image.asset('assets/police_icon_login.png'),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 18, bottom: 14),
                    child: Text(
                      "LOGIN MASYARAKAT APP",
                      style: blackTextFont.copyWith(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 23),
                    child: Text(
                      "Silahkan masuk ke Lapor Masyarakat App untuk\nmelaporkan kegiatan kriminal, kasus covid,\nkecelakaan lalu lintas dan kebakaran di sekitar\nanda. ",
                      style: blackTextFont.copyWith(
                        fontSize: 13,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  TextField(
                    keyboardType: TextInputType.name,
                    onChanged: (value) {
                      setState(() {
                        isNameValid = true;
                      });
                    },
                    controller: nameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Masukkan Nama",
                    ),
                  ),
                  SizedBox(height: 16),
                  TextField(
                    keyboardType: TextInputType.name,
                    onChanged: (value) {
                      setState(() {
                        password = value;
                      });
                    },
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Masukkan Password",
                    ),
                  ),
                  SizedBox(height: 16),
                  TextField(
                    keyboardType: TextInputType.name,
                    onChanged: (value) {
                      setState(() {
                        (password == value)
                            ? isPasswordValid = true
                            : isPasswordValid = false;
                      });
                    },
                    controller: confirmPasswordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Konfirmasi Password",
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          if (isNameValid && isPasswordValid) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => MainPage()),
            );
          } else {
            Flushbar(
              duration: Duration(milliseconds: 1500),
              flushbarPosition: FlushbarPosition.TOP,
              backgroundColor: Color(0xFFFF5C83),
              message: "Data kamu tidak cocok",
            )..show(context);
          }
        },
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: (isNameValid && isPasswordValid)
                ? accentColor1
                : Color(0xFFE4E4E4),
          ),
          child: Center(
            child: Text(
              'LOGIN',
              style: blackTextFont.copyWith(
                color: (isNameValid && isPasswordValid)
                    ? Colors.white
                    : Color(0xFFBEBEBE),
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
