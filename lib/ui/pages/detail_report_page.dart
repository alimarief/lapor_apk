part of "pages.dart";

class DetailReportPage extends StatelessWidget {
  final Report report;

  DetailReportPage(this.report);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(defaultMargin, 20, defaultMargin, 0),
              child: Stack(
                children: <Widget>[
                  // HEADER
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back,
                        color: accentColor1,
                      ),
                    ),
                  ),
                  Center(
                    child: Column(
                      children: [
                        Text(
                          "DETAIL LAPORAN",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // DIVIDER
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: Divider(
                color: Color(0xFFE4E4E4),
                thickness: 1,
              ),
            ),
            //CONTENT
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        report.dateTime.dateAndTime,
                        style: blackTextFont.copyWith(fontSize: 14),
                      ),
                      Text(
                        getHourTime(dummyReports[0].dateTime),
                        style: blackTextFont.copyWith(fontSize: 14),
                      ),
                    ],
                  ),
                  SizedBox(height: 16),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.75,
                    child: Text(
                      report.title,
                      style: blackTextFont.copyWith(
                        fontSize: 25,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(height: 16),
                  (report.status == true)
                      ? Container(
                          height: 27,
                          width: 108,
                          decoration: BoxDecoration(
                            color: accentColor8,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Center(
                            child: Text(
                              "Sedang ditinjau",
                              style: orangeTextFont.copyWith(fontSize: 12),
                            ),
                          ),
                        )
                      : SizedBox(),
                  SizedBox(height: 16),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.75,
                    child: Text(
                      report.description,
                      style: blackTextFont.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  SizedBox(height: 27),
                  SizedBox(
                    height: 160,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: report.reportImage.length,
                      itemBuilder: (_, index) => Container(
                        margin: EdgeInsets.only(
                            left: (index == 0) ? 0 : 0,
                            right: (index == report.reportImage.length - 1)
                                ? defaultMargin
                                : 16),
                        child: DetailReportCard(report.reportImage[index]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          height: 55,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: accentColor2),
          child: Center(
            child: Text(
              'BATALKAN LAPORAN',
              style: blackTextFont.copyWith(
                color: Colors.white,
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
