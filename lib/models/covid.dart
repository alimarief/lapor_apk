part of 'models.dart';

class Covid extends Equatable {
  final int recover;
  final int died;
  final int suspect;

  Covid({
    this.recover,
    this.died,
    this.suspect,
  });

  // factory Covid.fromJson(Map<String, dynamic> json) => Covid(
  //       fID: json['FID'],
  //       kodeProvi: json['Kode_Provi'],
  //       provinsi: json['Provinsi'],
  //       kasusPosi: json['Kasus_Posi'],
  //       kasusSemb: json['Kasus_Semb'],
  //       kasusMeni: json['Kasus_Meni'],
  //     );

  @override
  List<Object> get props => [recover, died, suspect];
}

List<Covid> dummyCovid = [
  Covid(
    suspect: 5067,
    recover: 3842,
    died: 77,
  ),
];
