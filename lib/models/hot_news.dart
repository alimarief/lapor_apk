part of 'models.dart';

class HotNews extends Equatable {
  final String title;
  final String image;
  final DateTime date;

  HotNews({
    this.title,
    this.image,
    this.date,
  });

  @override
  List<Object> get props => [title, image, date];
}

List<HotNews> dummyHotNews = [
  HotNews(
    title:
        "Pemberantasan korupsi telah dilaksanakan secara merata setiap daerah",
    image: "assets/pict1.png",
    date: DateTime.now(),
  ),
  HotNews(
    title: "Penjualan iwak pitik dikota malang semakin banyak",
    image: "assets/pict1.png",
    date: DateTime.now(),
  ),
  HotNews(
    title: "Cafe Terbaik di kota malang",
    image: "assets/pict1.png",
    date: DateTime.now(),
  ),
];
