part of 'models.dart';

class Fire extends Equatable {
  final int hotSpot;
  final int fireSpot;
  final int fireArea;

  Fire({
    this.hotSpot,
    this.fireSpot,
    this.fireArea,
  });

  @override
  List<Object> get props => [hotSpot, fireSpot, fireArea];
}

List<Fire> dummyFire = [
  Fire(
    hotSpot: 11,
    fireSpot: 2,
    fireArea: 1002,
  ),
];
