part of 'models.dart';

class Report extends Equatable {
  final String title;
  final DateTime dateTime;
  final String description;
  final bool status;
  final List<String> reportImage;

  Report(
      {this.title,
      this.dateTime,
      this.description,
      this.status,
      this.reportImage});

  @override
  List<Object> get props => [title, dateTime, description, status];
}

List<Report> dummyReports = [
  Report(
    title: "Balapan liar di kawasan taman Rimba - Bandara Jambi",
    description:
        "Maraknya aksi kebut-kebutan atau balapan liar yang dilakukan oleh sejumlah pemuda, di sekitaran kawasan Taman Rimba, Kota Jambi, jalan menuju Bandara Sultan Thaha Jambi menjadi keluhan masyarakat terutama para pengguna jalan yang melintas. Hampir setiap harinya kawasan tersebut, dijadikan sirkuit dadakan balapan liar oleh sejumlah pemuda. Padahal, tak jarang pihak kepolisian setempat juga kerap melakukan penindakan dengan berpatroli dan menjaga serta membubarkan paksa balap liar tersebut.",
    dateTime: DateTime.now(),
    status: false,
    reportImage: [
      "assets/sepeda1.png",
      "assets/sepeda2.png",
      "assets/sepeda3.png"
    ],
  ),
  Report(
    title: "Penjual gorengan tawuran di alun - alun",
    description:
        "Penjualan gorengan semakin laku laris dipasaran tetapi juga ada yang tidak laku, para penjual gorengan berebut tempat jualan di daerah pinggir jalan mengakibatkan sakit hati dan akhirnya mereka tawuran",
    dateTime: DateTime.now(),
    status: true,
    reportImage: [
      "assets/sepeda1.png",
      "assets/sepeda2.png",
      "assets/sepeda3.png"
    ],
  ),
  Report(
    title: "Penjual cupang semakin marak",
    description:
        "Penjualan cupang adalah sosok bisnis baru yang marak pada saat pandemi terjadi. banyak orang yang berjualan cupang sebagai profesi utama maupun sampingan",
    dateTime: DateTime.now(),
    status: true,
    reportImage: [
      "assets/sepeda1.png",
      "assets/sepeda2.png",
      "assets/sepeda3.png"
    ],
  )
];
