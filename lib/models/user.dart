part of 'models.dart';

class User extends Equatable {
  // final int id;
  final String nik;
  final String name;
  final String email;
  final String phoneNumber;
  final String profilePicture;

  User(
      {this.email, this.name, this.nik, this.phoneNumber, this.profilePicture});

  @override
  List<Object> get props => [
        nik,
        phoneNumber,
        name,
        email,
        profilePicture,
      ];
}
